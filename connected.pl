connected(X, Y) :- path_to(Y, [X], []).

path_to(Y, [Y|_], _) :- !.
path_to(Y, [X|T], Used) :-
    findall(Z, (edge(X, Z), \+ member(Z, Used)), Neigh),
    append(T, Neigh, Queue),
    path_to(Y, Queue, [X|Used]).

