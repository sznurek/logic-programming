partition([], _, [], []).
partition([H|T], X, [H|A], B) :-
    H =< X,
    partition(T, X, A, B).
partition([H|T], X, A, [H|B]) :-
    H > X,
    partition(T, X, A, B).

mysort([], []).
mysort([E|X], Y) :-
    partition(X, E, A, B),
    mysort(A, As),
    mysort(B, Bs),
    append(As, [E|Bs], Y).
