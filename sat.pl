instantiate(X) :- var(X), !, (X = true; X = false).
instantiate(T) :-
    T =.. [_|Args],
    instantiate_list(Args).

instantiate_list([]).
instantiate_list([H|T]) :-
    instantiate(H),
    instantiate_list(T).

eval_formula(true).
eval_formula(or(X, Y)) :-
    eval_formula(X); eval_formula(Y).
eval_formula(and(X, Y)) :-
    eval_formula(X), eval_formula(Y).
eval_formula(neg(X)) :-
    \+ eval_formula(X).

sat(F) :-
    instantiate(F),
    eval_formula(F), !.
