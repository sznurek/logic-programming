bubsort2(X, X) :-
    msort(X, X), !.

bubsort2(X, Y) :-
    append(B, [X1,X2|E], X),
    X1 > X2,
    append(B, [X2,X1|E], Z),
    bubsort2(Z, Y).

bubsort(X, Y) :-
    bubsort2(X, Y).
