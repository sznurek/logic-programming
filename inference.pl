inference(Facts, Rules, MaxLength, Result) :-
    inference2(Rules, MaxLength, Result, Facts).

inference2(Rules, MaxLength, Result, Acc) :-
    inference_step(Acc, Rules, Result1),
    findall(T, (member(T, Result1), termsize(T, M), M =< MaxLength), Result2),
    append(Acc, Result2, Acc1),
    sort(Acc1, Acc2), % just to remove duplicates
    length(Acc, Len1),
    length(Acc2, Len2),
    (Len1 = Len2 -> Result = Acc; inference2(Rules, MaxLength, Result, Acc2)).

inference_step(Facts, Rules, Result) :-
    findall(Result1, (member(R, Rules), infer_all(R, Facts, Result1)), List),
    uniq_flatten(List, Result).

infer_all(Rule, Facts, Result) :-
    findall(Out, infer(Rule, Facts, Out), Result1),
    uniq_flatten(Result1, Result).

infer(In >> Out, Facts, Out1) :-
    copy_term(In >> Out, In1 >> Out1),
    subl(In1, Facts).

termsize(T, 1) :-
    var(T), !.

termsize(T, 1) :-
    functor(T, _, 0).

termsize(T, N) :-
    T =.. [_|Args],
    termsize_list(Args, N1),
    N is N1 + 1.

termsize_list([], 0).
termsize_list([H|T], N) :-
    termsize(H, N1),
    termsize_list(T, N2),
    N is N1 + N2.

uniq_flatten(Ls, L) :-
    flatten(Ls, L1),
    sort(L1, L).

subl([], _).
subl([X|H], L) :-
    member(X, L),
    subl(H, L).
