single_term(Signatures, Head/Arity, Term, Size) :-
    functor(Term, Head, Arity),
    Term =.. [Head|Args],
    ( var(Size) ->
        term_list(Signatures, Args, Size1),
        Size is Size1 + 1
    ;
        Size > 0,
        Size1 is Size - 1,
        term_list_gen(Signatures, Args, Size1)
    ).

term_list(_, [], 0).
term_list(Signatures, [H|T], S) :-
    term(Signatures, S1, H),
    term_list(Signatures, T, S2),
    S is S1 + S2.

term_list_gen(_, [], 0).
term_list_gen(Signatures, [H|T], S) :-
    partition(S1, S2, S),
    S1 > 0,
    term(Signatures, S1, H),
    term_list_gen(Signatures, T, S2).

term(Signatures, S, T) :-
    member(Sig, Signatures),
    single_term(Signatures, Sig, T, S).

partition(A, B, N) :-
    partition(A, B, N, N).

partition(A, B, A, N) :-
    B is N - A.

partition(A, B, C, N) :-
    C >= 1,
    C1 is C - 1,
    partition(A, B, C1, N).

