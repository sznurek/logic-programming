sum_list(X, N) :- sum_list(X, N, 0).
sum_list([], N, N).
sum_list([H|T], N, A) :-
    A1 is A + H,
    sum_list(T, N, A1).
