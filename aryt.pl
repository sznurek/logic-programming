:- use_module(library(clpfd)).

chars_value([X], '*'(X, Base), X, Base).

chars_value([X,Y|Xs], Value, MSD, Base) :-
    NextBase is Base * 10,
    chars_value([Y|Xs], RecValue, MSD, NextBase),
    Value = '+'('*'(X, Base), RecValue).

riddle_constraint(One + Two = Three) :-
    chars_value(One, OneValue, MSD1, 1),
    chars_value(Two, TwoValue, MSD2, 1),
    chars_value(Three, ThreeValue, MSD3, 1),
    MSD1 #\= 0 #/\ MSD2 #\= 0 #/\ MSD3 #\= 0 #/\ OneValue + TwoValue #= ThreeValue.

update_mapping(X, Mapping, NewMapping) :-
    (get_assoc(X, Mapping, _), NewMapping = Mapping, !;
     put_assoc(X, Mapping, _, NewMapping)).

generate_mapping(Chars, Result) :-
    foldl(update_mapping, Chars, t, Result).

get_assoc_p(K, A, V) :- get_assoc(A, K, V).

map_chars_to_vars(Assoc, Chars, Vars) :-
    maplist(get_assoc_p(Assoc), Chars, Vars).

revchars(Atom, RevChars) :-
    atom_chars(Atom, Chars),
    reverse(Chars, RevChars).

preprocess_riddle(One + Two = Three, This + That = Other, ResultMapping) :-
    In = [One, Two, Three],
    Out = [This, That, Other],
    maplist(revchars, In, ProcessedIn),
    append(ProcessedIn, AllChars),
    generate_mapping(AllChars, ResultMapping),
    maplist(map_chars_to_vars(ResultMapping), ProcessedIn, Out).

convert_result(X-Y, (X, Y)).

aryt(Riddle, Result) :-
    preprocess_riddle(Riddle, ProcessedRiddle, Mapping),
    assoc_to_values(Mapping, Vars),
    all_different(Vars),
    Vars ins 0..9,
    riddle_constraint(ProcessedRiddle),
    labeling([ffc, down, bisect], Vars),
    assoc_to_list(Mapping, Result1),
    maplist(convert_result, Result1, Result).
