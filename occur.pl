occurences(S, R, 1) :- S == R, !.
occurences(S, T, N) :-
    T =.. [_|Args],
    occurences_list(S, Args, N, 0).

occurences_list(_, [], A, A).
occurences_list(S, [H|T], N, A) :-
    occurences(S, H, N1),
    A1 is N1 + A,
    occurences_list(S, T, N, A1).
