:- use_module(library(clpfd)).

:- dynamic(fill_memo/3).

nono(Rows, Columns, Result) :-
    length(Rows, N),
    length(Columns, M),
    generate(N, M, Board),
    clpfd:transpose(Board, TBoard),
    maplist(annotate_row(M), Board, Rows, AnnBoard),
    maplist(annotate_row(N), TBoard, Columns, AnnTBoard),
    reconstraint_board(AnnBoard, AnnTBoard),
    solve(Board, TBoard, AnnBoard, AnnTBoard),
    maplist(maplist(format_change), Board, Result), !.

annotate_row(N, Row, Cs, (Row, Poss)) :-
    findall(L, possible_row(N, Cs, L), Poss).

solve(Board, TBoard, AnnBoard, AnnTBoard) :-
    (board_solved(Board) ->
         true;
         guess(AnnBoard, I, J),
         reconstraint_board([I], [J], AnnBoard, AnnTBoard),
         solve(Board, TBoard, AnnBoard, AnnTBoard)
    ).

board_solved(Board) :-
    term_variables(Board, []).

guess(Board, I, J) :-
    nth0(I, Board, (Row, _)),
    nth0(J, Row, Cell),
    var(Cell), !,
    member(Cell, [full, empty]).

format_change(full, 1).
format_change(empty, 0).

draw([]).
draw([R|Rs]) :-
    draw_row(R), nl,
    draw(Rs).

draw_row([]).
draw_row([X|Cs]) :-
    var(X), !,
    write(' '),
    draw_row(Cs).
draw_row([full|Cs]) :-
    write('X'),
    draw_row(Cs).
draw_row([empty|Cs]) :-
    write('.'),
    draw_row(Cs).

prefix(0, []) :- !.
prefix(N, Xs) :-
    N1 is N - 1,
    prefix(N1, Ys),
    append(Ys, [N1], Xs).

reconstraint_board(AnnRows, AnnCols) :-
    length(AnnRows, N),
    prefix(N, Initial),
    length(AnnCols, M),
    prefix(M, InitialCols),
    reconstraint_board(Initial, InitialCols, AnnRows, AnnCols).

reconstraint_board(Xs, Ys, AnnRows, AnnCols) :-
    reconstraint_board(Xs, AnnRows, AnnCols),
    reconstraint_board(Ys, AnnCols, AnnRows).

reconstraint_board([], _, _) :- !.

reconstraint_board(Changed, AnnRows, AnnCols) :-
    filter_rows(Changed, AnnRows, FilteredAnnRows),
    reconstraint_rows(FilteredAnnRows, FilteredAnnRows1, Changed1),
    replace_rows(Changed, AnnRows, FilteredAnnRows1, AnnRows1),
    reconstraint_board(Changed1, AnnCols, AnnRows1).

replace_rows(Changed, OldRows, NewRows, Result) :-
    replace_rows(Changed, OldRows, NewRows, 0, Result).

replace_rows([], OldRows, [], _, OldRows) :- !.
replace_rows([I|Ix], [_|OldRows], [R|NewRows], I, [R|Result]) :-
    !, N is I + 1,
    replace_rows(Ix, OldRows, NewRows, N, Result).

replace_rows(Ix, [R|OldRows], NewRows, N, [R|Result]) :-
    N1 is N + 1,
    replace_rows(Ix, OldRows, NewRows, N1, Result).

filter_rows([], _, []).
filter_rows([I|Ix], Rows, [Row|Result]) :-
    nth0(I, Rows, Row),
    filter_rows(Ix, Rows, Result).

reconstraint_rows(AnnRows, NewAnnRows, Changed) :-
    AnnRows = [(R,_)|_],
    length(R, N),
    reconstraint_rows(N, AnnRows, NewAnnRows, [], [], Changed).

reconstraint_rows(_, [], NewAnnRows, Acc, Changed, Changed) :-
    reverse(Acc, NewAnnRows).

reconstraint_rows(N, [R|Rs], NewAnnRows, NewAcc, ChangedAcc, ChangedResult) :-
    reconstraint(N, R, Changed, R1), !,
    append(Changed, ChangedAcc, ChangedAcc1),
    sort(ChangedAcc1, NewChangedAcc),
    reconstraint_rows(N, Rs, NewAnnRows, [R1|NewAcc], NewChangedAcc, ChangedResult).

unifies(X, Y) :-
    \+ \+ X = Y.

reconstraint(N, (Row, Poss), Changed, (Row, Poss1)) :-
    include(unifies(Row), Poss, Poss1),
    copy_term(Row, RowCopy),
    fold_common(N, Poss1, Row),
    calc_diff(Row, RowCopy, Changed).

calc_diff(Row1, Row2, Result) :-
    calc_diff(Row1, Row2, [], 0, Result1),
    reverse(Result1, Result).

calc_diff([], [], Acc, _, Acc).
calc_diff([R1|Rs1], [R2|Rs2], Acc, N, Result) :-
    N1 is N + 1,
    ( ((var(R1), \+ var(R2)) ; (var(R2), \+ var(R1)) ; R1 \= R2 ) ->
          calc_diff(Rs1, Rs2, [N|Acc], N1, Result);
          calc_diff(Rs1, Rs2, Acc, N1, Result)).

fold_common(N, Ls, R) :-
    length(Init, N),
    fill(Init, null, N),
    foldl(common, Ls, Init, R).

common(L1, L2, L) :-
    maplist(conjunction, L1, L2, L).

conjunction(X, Y, _) :- (var(X); var(Y)), !.
conjunction(null, X, X) :- !.
conjunction(X, null, X) :- !.
conjunction(X, X, X) :- !.
conjunction(_, _, _).

possible_row(N, Cs, L) :-
    foldl(sum_num, Cs, 0, S),
    length(Cs, Len),
    Space is Len + S - 1,
    length(L, N),
    possible_row(N, Space, Cs, L).

possible_row(N, _, [], L) :-
    fill(L, empty, N).

possible_row(N, S, [_], L) :-
    N =:= S,
    fill(L, full, N), !.

possible_row(N, S, [H|T], L) :-
    N >= S,
    H1 is H + 1,
    split_at(H, Filled, [empty|L1], L),
    fill(Filled, full, H),
    S1 is S - H1,
    N1 is N - H1,
    possible_row(N1, S1, T, L1).

possible_row(N, S, [H|T], [empty|L]) :-
    N >= S,
    N1 is N - 1,
    possible_row(N1, S, [H|T], L).

fill(_, _, 0).
fill([C|T], C, N) :-
    N1 is N - 1,
    fill(T,C,N1).

fill_fresh([], 0).
fill_fresh([_|T], N) :-
    N1 is N - 1,
    fill_fresh(T, N1).

generate(N, M, Board) :-
    length(Board, N),
    maplist(flength(M), Board).

split_at(0, [], L, L) :- !.

split_at(N, [H|F], L, [H|T]) :-
    N1 is N - 1,
    split_at(N1, F, L, T).

sum_num(X, Y, Z) :- Z is X + Y.
flength(N, L) :- length(L, N).
felement(Vs, N, V) :- nth0(N, Vs, V).
