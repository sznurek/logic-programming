append([], X, X).
append([H|T], X, [H|R]) :-
    append(T, X, R).

reverse([], []).
reverse([H|T], R) :-
    reverse(T, R1),
    append(R1, [H], R).


prefix(X, Y) :-
    append(X, _, Y).

suffix(X, Y) :-
    append(_, X, Y).

sublist([], _).

sublist(X, Y) :-
    prefix(Y1, Y),
    suffix(X, Y1),
    \+ X = [].

member(X, Y) :- sublist([X], Y).

member2(X, Y) :- append(_, [X|_], Y).

adjacent(X, Y, L) :- append(_, [X|[Y|_]], L).

last(X, L) :- append(_, [X], L).

% LISP!

writeLisp(T) :-
    functor(T, Head, 0),
    write(Head).

writeLisp(T) :-
    functor(T, _, N),
    N > 0,
    T =.. [_|Args],
    write('('),
    writeArgs(Args),
    write(')').

writeArgs([]).
writeArgs([A]) :-
    writeLisp(A), !.
writeArgs([A|As]) :-
    writeLisp(A),
    write(' '),
    writeArgs(As).

% Shuffle!

shuffle_term(T, R) :-
    functor(T, _, N), N >= 2,
    T =.. [Head|Args],
    shuffle_pair(Args, Args1),
    R =.. [Head|Args1].

shuffle_term(T, R) :-
    T =.. [Head|Args],
    apply_on_one(Args, Args1),
    R =.. [Head|Args1].

apply_on_one([X|Xs], [X1|Xs]) :-
    functor(X, _, N), N >= 2,
    shuffle_term(X, X1).
apply_on_one([X|Xs], [X|Xs1]) :-
    apply_on_one(Xs, Xs1).

shuffle_pair(X, Y) :-
    append(T, [H|R], X),
    swap(H, S, R, Z),
    append(T, [S|Z], Y).

swap(X, Y, [Y|T], [X|T]).
swap(X, Y, [H|T], [H|T1]) :-
    swap(X, Y, T, T1).

