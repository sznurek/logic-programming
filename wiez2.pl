:- use_module(library(clpfd)).

wiezowce(N, Rows, Cols, Board) :-
    board(N, Board, TBoard),
    maplist(constraint_row_visibility(N), Board, Rows),
    maplist(constraint_row_visibility(N), TBoard, Cols),
    term_variables(Board, AllVars),
    labeling([], AllVars).

board(N, Board, TBoard) :-
    length(Board, N),
    maplist(constraint_row(N), Board),
    transpose(Board, TBoard),
    maplist(constraint_row(N), TBoard).

constraint_row(N, Row) :-
    length(Row, N),
    Row ins 1..N,
    all_different(Row).

constraint_row_visibility(N, Row, (L, R)) :-
    reverse(Row, RRow),
    automaton_nodes(N, Nodes),
    automaton_arcs(N, Nodes, C, Arcs),
    exclude(internal, Nodes, RealNodes),
    automaton(_, _, Row, RealNodes, Arcs, [C], [0], [L1]),
    automaton(_, _, RRow, RealNodes, Arcs, [C], [0], [R1]),
    L1 #= L,
    R1 #= R.

automaton_nodes(N, [source(node(0, 0))|Nodes]) :-
    enum(1, N, L),
    findall(Node,
      (member(Maxi, L),
       member(Step, L),
       Maxi >= Step,
       create_node(N, Maxi, Step, Node)), Nodes).

create_node(N, N, N, sink(node(N, N))) :- !.
create_node(_, Maxi, Step, internal(node(Maxi, Step))).

internal(internal(_)).

unwrap_node(source(X), X).
unwrap_node(sink(X), X).
unwrap_node(internal(X), X).

automaton_arcs(N, Nodes, Cnt, Arcs) :-
    enum(1, N, L),
    findall(arc(node(Maxi1, Step1), K, node(Maxi2, Step2), [C]),
      (member(N1, Nodes),
       member(N2, Nodes),
       member(K, L),
       unwrap_node(N1, node(Maxi1, Step1)),
       unwrap_node(N2, node(Maxi2, Step2)),
       Step2 is Step1 + 1,
       (K < Maxi1 -> Maxi2 = Maxi1
                  ; Maxi1 < Maxi2, Maxi2 = K),
       (K > Maxi1 -> C = Cnt + 1; C = Cnt)
       ),
      Arcs),
    term_variables(Arcs, Vars),
    maplist('='(Cnt), Vars).

enum(Start, Stop, Result) :-
    enum(Start, Stop, Result, [], Stop).

enum(Start, _, [Start|Result], Result, Start) :- !.
enum(Start, Stop, Result, Acc, N) :-
    N1 is N-1,
    enum(Start, Stop, Result, [N|Acc], N1).
