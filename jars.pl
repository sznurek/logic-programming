empty_queue(queue([], [])).
queue_push(queue(X, Y), E, queue(X, [E|Y])).
queue_pop(queue([], Y), E, Result) :-
    reverse(Y, X),
    queue_pop(queue(X, Y), E, Result).

queue_pop(queue([E|X], Y), E, queue(X, Y)).

select1(L, E, rest(X,Y)) :-
    append(X, [E|Y], L).

select2(L, E1, E2, rest(X1,X2,X3)) :-
    append(R, [E2|X3], L),
    append(X1, [E1|X2], R).

fill_jar(jar(Cap, _), jar(Cap, Cap)).
empty_jar(jar(Cap, ), jar(Cap, 0)).
