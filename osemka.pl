manhattan(((X1, Y1), (X2, Y2)), D) :-
    D is abs(X1 - X2) + abs(Y1 - Y2).

valid_move(P, Q) :-
    manhattan((P, Q), 1).

exchange(Pos1, Pos2, (Pos1, K), (Pos2, K)) :- !.
exchange(Pos1, Pos2, (Pos2, K), (Pos1, K)) :- !.
exchange(_, _, P, P).

swap_pos(State, Pos1, Pos2, Result) :-
    maplist(exchange(Pos1, Pos2), State, Result).

valid_moves(State, Result) :-
    member((HolePos, o), State),
    findall(Pos, (member((Pos, _), State), valid_move(HolePos, Pos)), Moves),
    maplist(swap_pos(State, HolePos), Moves, Result1),
    maplist(sort, Result1, Result).

positions([(1,1), (1,2), (1,3), (2,1), (2,2), (2,3), (3,1), (3,2), (3,3)]).

make_pair(X, Y, (X, Y)).

annotate(State, CState) :-
    positions(Pos),
    maplist(make_pair, Pos, State, CState).

snd((_, X), X).

unannotate(CState, State) :-
    sort(CState, MState),
    maplist(snd, MState, State).

:- dynamic(visited/1).

sum(X, Y, Z) :- Z is X + Y.

heuristic(Target, CState, Cost) :-
    findall((Pos1, Pos2), (member((Pos1, K), Target), member((Pos2, K), CState)),
            Positions),
    maplist(manhattan, Positions, Costs),
    foldl(sum, Costs, 0, Cost).

bfs_cost(Target, (D, CS), Cost) :-
    heuristic(Target, CS, Cost1),
    Cost is Cost1 + 0.4*D.

su((X, C, Y),(X, C, Z)) :- unannotate(Y, Z).

write_queue(Target, Q) :-
    maplist(su(Target), Q, Q1),
    write(Q1).

annotate_move(Target, D, State, (C, D, State)) :-
    bfs_cost(Target, (D, State), C).

insert_all(H, [], H).
insert_all(H, [(C, D, State)|Rest], H1) :-
    add_to_heap(H, C, (D, State), H0),
    insert_all(H0, Rest, H1).

bfs(H, Target, N) :-
    get_from_heap(H, _, Head, H0),
    bfs(Head, H0, Target, N).

bfs((D, CState), _, CState, D) :- !.

bfs((_, CState), Rest, Target, N) :-
    visited(CState),
    bfs(Rest, Target, N).

bfs((D, CState), Rest, Target, N) :-
    \+ visited(CState),
    asserta(visited(CState)),
    valid_moves(CState, Moves),
    D1 is D + 1,
    maplist(make_pair(D1), Moves, QueueTail1),
    append(Rest, QueueTail, Queue),
    bfs(Queue, Target, N).

osemka(State1, State2, N) :-
    retractall(visited(_)),
    annotate(State1, CState1),
    annotate(State2, CState2),
    bfs_cost(CState2, (0, CState1), C),
    empty_heap(H0),
    add_to_heap(H0, C, (0, CState1), H),
    bfs(H, CState2, N).
