game_ended(state([], _, _, _)) :- !.
game_ended(state(_, [], _, _)).

step(state([X|L1], [Y|L2], Buffer, hidden), state(L1, L2, [Y,X|Buffer], start)).
step(state([X|L1], [X|L2], Buffer, start), state(L1, L2, [X,X|Buffer], hidden)).
step(state([X|L1], [Y|L2], Buffer, start), state(R1, L2, [], start)) :-
    X > Y, !,
    reverse([Y,X|Buffer], Won),
    append(L1, Won, R1).
step(state([X|L1], [Y|L2], Buffer, start), state(L1, R2, [], start)) :-
    X < Y,
    reverse([Y,X|Buffer], Won),
    append(L2, Won, R2).

inc(inf, inf) :- !.
inc(X, Y) :- Y is X + 1.

%play(S, _) :- write(S), nl, fail.
play(S, 0) :- game_ended(S), !.
play(S, inf) :- play_cache(S), !.
play(S, N) :-
    assertz(play_cache(S)),
    step(S, S1),
    play(S1, N1),
    inc(N1, N).

war(L1, L2, N) :-
    retractall(play_cache(_)), % sideeffect: declares play_cache/1 as dynamic
    play(state(L1, L2, [], start), N).
