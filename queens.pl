:- use_module(library(clpfd)).

not_colliding((X1, Y1), (X2, Y2), '#\\='(abs(X1 - X2), abs(Y1 - Y2))).
and_it(X, Y, '#/\\'(X, Y)).

all_not_colliding(Point, L, Term) :-
    maplist(not_colliding(Point), L, Terms),
    foldl(and_it, Terms, 1 #= 1, Term).

queens(N, L) :-
    length(L, N),
    L ins 1..N,
    all_different(L),
    queens(L, 1, []),
    labeling([ff], L).

queens([H|T], I, Used) :-
    all_not_colliding((I, H), Used, Term),
    Term,
    I1 is I + 1,
    queens(T, I1, [(I, H)|Used]).

queens([], _, _).
