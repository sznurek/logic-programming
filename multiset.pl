increase(void, X, bag(X, 1, void)).
increase(bag(X, N, T), X, bag(X, N1, T)) :-
    !, N1 is N + 1.
increase(bag(H, N, T), X, bag(H, N, T1)) :-
    increase(T, X, T1).

convert([], void).
convert([X|T], Y) :-
    convert(T, Y1),
    increase(Y1, X, Y).

