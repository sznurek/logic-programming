opposite(N, - N).
opposite(- N, N).

pairs(L, A, B) :-
    append(_, [A|L1], L),
    append(_, [B|_], L1).

resolve(clause(Literals1, Proof1), clause(Literals2, Proof2), clause(Literals, Proof)) :-
    member(N, Literals1),
    member(M, Literals2),
    opposite(N, M),
    del(N, Literals1, RLiterals1),
    del(M, Literals2, RLiterals2), !,
    append(RLiterals1, RLiterals2, DirtyLiterals),
    sort(DirtyLiterals, Literals),
    Proof = resolvent(Literals, Proof1, Proof2).

trivial(clause(L, _)) :-
    member(A, L),
    member(B, L),
    opposite(A, B).

has_clause(clause(Literals1, _), [clause(Literals2, _)|_]) :-
    forall(member(X, Literals2), member(X, Literals1)), !.
has_clause(C, [_|Cs]) :- has_clause(C, Cs).

valid_resolvent(Clauses, Clause) :-
    pairs(Clauses, Clause1, Clause2),
    resolve(Clause1, Clause2, Clause),
    \+ trivial(Clause).

filter_clauses([], Clauses, Clauses).
filter_clauses([C|Cs], Clauses, Result) :-
    filter_clauses(Cs, Clauses, Result1),
    (has_clause(C, Result1) -> Result = Result1; Result = [C|Result1]).

resolution_aux(Clauses, Proof) :-
    findall(Clause, valid_resolvent(Clauses, Clause), Resolvents),
    filter_clauses(Resolvents, Clauses, NewClauses),
    decide(Clauses, NewClauses, Proof).

decide(_, Clauses, Proof) :-
    member(clause([], Proof), Clauses), !.

decide(OldClauses, NewClauses, Proof) :-
    \+ (length(OldClauses, N), length(NewClauses, N)), !,
    resolution_aux(NewClauses, Proof).

initial_proofs([], []).
initial_proofs([C|Cs], [clause(C1, axiom(C1))|Cs1]) :-
    sort(C, C1),
    initial_proofs(Cs, Cs1).

resolution(Clauses, Proof) :-
    initial_proofs(Clauses, ProvenClauses),
    resolution_aux(ProvenClauses, Proof).
