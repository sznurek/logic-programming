make_tree([], T, T).
make_tree([H|T], Tree, Result) :-
    get_assoc(H, Tree, Old, Tree1, New), !,
    New is Old + 1,
    make_tree(T, Tree1, Result).

make_tree([H|T], Tree, Result) :-
    put_assoc(H, Tree, 1, Tree1),
    make_tree(T, Tree1, Result).

to_bag([], void).
to_bag([(X,C)|T], bag(X, C, Bag)) :-
    to_bag(T, Bag).

convert(List, Bag) :-
    make_tree(List, t, Tree),
    findall((X, C), gen_assoc(X, Tree, C), Pairs),
    to_bag(Pairs, Bag).
