wygrywa(L, K) :-
    asserta(wygrywa_cache([], 0)),
    wygrywa_aux(L, K).

wygrywa_aux(L, K) :- wygrywa_cache(L, K), !.
wygrywa_aux(L, K) :- member(N, L), N >= K, !.
wygrywa_aux(L, K) :-
    member(N, L),
    K1 is K - N,
    \+ wygrywa_aux(L, K1),
    asserta(wygrywa_cache(L, K)).

