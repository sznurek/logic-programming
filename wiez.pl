wiezowce(N, Rows, Cols, Result) :-
    length(Result, N),
    maplist(flength(N), Result),
    transpose(Result, TResult),
    precompute_perms(N, PermMap),
    %maplist(quick_help(N), Rows, Result),
    %maplist(quick_help(N), Cols, TResult), !,
    generate_rows(N, PermMap, Result, Rows, TResult, Cols, PermMap, PermMap),
    maplist(valid_row(PermMap), Rows, Result),
    maplist(valid_row(PermMap), Cols, TResult).

%quick_help(N, (L, R), Row) :-
%    (L = 1 -> Row = [N|_]; true),
%    (R = 1 -> reverse(Row, [N|_]); true),
%    (L = N -> enum(1, N, Row); true),
%    (R = N -> reverse(Row, RRow), enum(1, N, RRow); true).

flength(N, L) :- length(L, N).

generate_rows(_, _, Rows, _, _, _, _, _) :-
    term_variables(Rows, []), !.

generate_rows(N, PM, Rows, RowsVis, Cols, ColsVis, PermMapRows, PermMapCols) :-
    relaxate(N, PM, Rows, RowsVis, Cols, ColsVis, PermMapRows, PermMapRows1),
    generate_rows(N, PM, Rows, RowsVis, Cols, ColsVis, PermMapRows1, PermMapCols).
    %generate_rows(N, Cols, ColsVis, Rows, RowsVis, PermMapCols, PermMapRows1).

relaxate(_, PM, Rows, RowsVis, Cols, ColsVis, PermMap, PermMap1) :-
    pairs(Rows, RowsVis, (Row, Vis)),
    \+ term_variables(Row, []), !,
    valid_row(PermMap, Vis, Row),
    visibility_not_broken(PM, ColsVis, Cols),
    filter_rows(PermMap, Row, PermMap1).

%write_board([]) :- nl.
%write_board([R|Rs]) :-
%    write_row(R), nl,
%    write_board(Rs).

%write_row([]) :- nl.
%write_row([C|Cs]) :-
%    var(C), !, write('  '),
%    write_row(Cs).
%write_row([N|Cs]) :-
%    write(N), write(' '),
%    write_row(Cs).

pairs([X|_], [Y|_], (X, Y)).
pairs([_|Xs], [_|Ys], P) :-
    pairs(Xs, Ys, P).

visibility_not_broken(PermMap, Rows, Vis) :-
    maplist(visibility_not_broken_one(PermMap), Vis, Rows).

visibility_not_broken_one(PermMap, Row, Vis) :-
    \+ \+ valid_row(PermMap, Vis, Row).

%visibility_not_broken(N, Vis, Rows) :-
%    maplist(visibility_not_broken_one(N), Vis, Rows), !.

%visibility_not_broken_one(N, (L, R), Row) :-
%    concrete_prefix(Row, ConcreteRow),
%    visibility(ConcreteRow, L1),
%    L1 =< L,
%    maybe_visible(N, ConcreteRow, K),
%    L1 + K >= L,
%    maximum_not_in(N, ConcreteRow, Max),
%    reverse(ConcreteRow, RConcreteRow),
%    visibility([Max|RConcreteRow], R1),
%    R1 =< R,
%    length(ConcreteRow, M),
%    R1 + N - M >= R.

maximum_not_in(N, List, M) :-
    predsort(rcompare, List, SList),
    maximum_not_in_aux(N, SList, M).

rcompare(X, Y, Z) :- compare(X, Z, Y).

maximum_not_in_aux(N, [N|Xs], M) :-
    !, N1 is N - 1,
    maximum_not_in_aux(N1, Xs, M).

maximum_not_in_aux(N, _, N).

maybe_visible(N, Xs, K) :-
    foldl(maximum, Xs, 0, Max),
    K is N - Max.

maximum(X, Y, Z) :-
    Z is max(X, Y).

concrete_prefix([], []).
concrete_prefix([X|Xs], [X|Ys]) :-
    \+ var(X), !,
    concrete_prefix(Xs, Ys).
concrete_prefix(_, []).

filter_rows(PermMap, Row, PermMap1) :-
    map_assoc(filter_rows_aux(Row), PermMap, PermMap1).

filter_rows_aux(Row, Perms, NewPerms) :-
    exclude(intersects(Row), Perms, NewPerms).

valid_row(PermMap, RowVis, Row) :-
    get_assoc(RowVis, PermMap, Rows),
    member(Row, Rows).

intersects([X|_], [X|_]) :- \+ var(X).
intersects([_|Xs], [_|Ys]) :-
    intersects(Xs, Ys).

transpose([[X]|Xss], [Yss]) :-
    !, flatten([[X]|Xss], Yss).

transpose(Xss, [Col|Yss]) :-
    foldl(collect_column, Xss, (Z1-Z1, Z2-Z2), (Col-[], Rest-[])),
    transpose(Rest, Yss).

collect_column([X|Xs], (Col-[X|ColT], Rest-[Xs|RestT]), (Col-ColT, Rest-RestT)).

precompute_perms(N, PermsMap) :-
    findall((Vis-Perm), (perms(N, Perm), two_visibility(Perm, Vis)), Perms),
    list_to_assoc_set(Perms, PermsMap).

list_to_assoc_set(List, Result) :-
    empty_assoc(A),
    list_to_assoc_set(List, Result, A).

list_to_assoc_set([], A, A).

list_to_assoc_set([(K-V)|Xs], Result, A0) :-
    get_assoc(K, A0, Vs),
    put_assoc(K, A0, [V|Vs], A), !,
    list_to_assoc_set(Xs, Result, A).

list_to_assoc_set([(K-V)|Xs], Result, A0) :-
    put_assoc(K, A0, [V], A),
    list_to_assoc_set(Xs, Result, A).

two_visibility(Xs, (L, R)) :-
    visibility(Xs, L),
    reverse(Xs, Ys),
    visibility(Ys, R).

visibility(Xs, N) :-
    foldl(visibility_aux, Xs, (0,0), (N, _)).

visibility_aux(H, (Cnt, Max), (Cnt1, Max1)) :-
    Max1 is max(Max, H),
    (H > Max -> Cnt1 is Cnt + 1
              ; Cnt1 = Cnt).

perms(N, Ys) :-
    enum(1, N, Xs),
    permutation(Xs, Ys).

enum(Start, Stop, Result) :-
    enum(Start, Stop, Result, [], Stop).

enum(Start, _, [Start|Result], Result, Start) :- !.
enum(Start, Stop, Result, Acc, N) :-
    N1 is N-1,
    enum(Start, Stop, Result, [N|Acc], N1).

%tests :-
%    write('enum'), nl,
%    enum(2, 7, [2,3,4,5,6,7]),

%    write('perms'), nl,
%    findall(P, perms(2, P), [[1,2],[2,1]]),

%    write('visibility'), nl,
%    visibility([1,2,3,5,4,7,6], 5),

%    write('two_visibility'), nl,
%    two_visibility([1,2,3,5,4,7,6], (5, 2)),

%    write('transpose'), nl,
%    transpose([[1,2,3],[4,5,6],[7,8,9]], [[1, 4, 7], [2, 5, 8], [3, 6, 9]]),

%    write('intersects'), nl,
%    \+ intersects([1,2,3], [4,5,6]),
%    intersects([1,2,3], [3,2,1]),
%    \+ intersects([1,2,3], [3,1,2]),

%    write('filter_rows'), nl,
%    precompute_perms(2, PermMap),
%    filter_rows(PermMap, [1, 2], PermMap1),
%    assoc_to_list(PermMap1, L),
%    permutation(L, [(1,2)-[[2,1]], (2,1)-[]]),

%    write('Concrete prefix'), nl,
%    concrete_prefix([1,2,_,_], [1,2]),
%    concrete_prefix([_, _], []),

%    write('maximum not in'), nl,
%    maximum_not_in(8, [4,3,6,1,8], 7),
%    maximum_not_in(8, [1,6,5,3,5], 8).
